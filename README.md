# QQunsolitaire
一个秒抢QQ群接龙的脚本  
`.exe`文件下载:
* Github:[点击下载](https://github.com/TuanKay10/qqunsolitaire/releases/download/qqunsolitaire_v1/qqunsolitaire_win_x64.zip)
* Gitee:[点击下载](https://gitee.com/elaina520/qqunsolitaire/attach_files/977921/download/qqunsolitaire_win_x64.zip)
# Cookie的获取
```
javascript:(function(){let domain=document.domain;let cookie=document.cookie;prompt('Cookies: '+domain, cookie)})();
```
* 浏览器登录[QQ空间](https://i.qq.com/)
* 将以上代码直接复制进入地址输入栏，回车即可弹出Cookie获取窗口   
![image](./image/GetCookie1.png)
![image](./image/GetCookie2.png)
# 配置文件
```
{
    "cookies" : "",
    "gid" : ""
}
```
* 将上文获取的`cookies`填入`config.json`对应位置中，`gid`表示需要接龙的群号  
# 声明
使用 QQunsolitaire  即表明，您知情并同意：  
* 该项目仅供学习交流，严禁用于商业用途  
* QQunsolitaire 不会对您的任何损失负责，包括但不限于账号异常、核弹爆炸、第三次世界大战等
